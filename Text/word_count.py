import re
pattern = re.compile(r'[A-z0-9]+')
global_count = dict()


def sanitize_and_record(arr):
    for x in arr:
        word = pattern.search(x).group()
        if word in global_count:
            global_count[word] += 1
        else:
            global_count[word] = 1


def count(input):
    input_arr = input.split(" ")
    sanitize_and_record(input_arr)
    return len(input_arr)

def main():
    f = open('wc.txt')
    for i in f:
        wc = count(i)

    print "Global Word Count: ", wc
    for i in global_count:
        print i, global_count[i]


if __name__ == '__main__':
    main()