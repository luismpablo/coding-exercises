import re


pattern_string = "".join([
    r'(?P<n1>[0-9]+(?:\.[0-9]*)?)?\s*',
    r'(?P<op>[+|\-|*|/])?\s*',
    r'(?P<n2>[0-9]+(?:\.[0-9]*)?)?'])

pattern = re.compile(pattern_string)

def run_op(op = None, n1 = None, n2 = None):
    # print "runop:", op, n1, n2
    if op == "+":
        return n1 + n2
    elif op == '-':
        return n1 - n2
    elif op == '*':
        return n1 * n2
    elif op == '/':
        return n1 / n2
    else:
        return float('NaN')

def main():
    memory = []
    while True:
        # print memory
        str_test = raw_input('>> ')
        match = pattern.search(str_test)

        n1 = match.group('n1')
        n2 = match.group('n2')
        op = match.group('op')

        if n1 != None:
            # n1 <op> ..
            if op != None:
                # n1 <op> n2
                if n2 != None:
                    ans = run_op(op, float(n1), float(n2))

                    if len(memory) == 1:
                        memory = []
                    elif len(memory) == 2:
                        last_op = memory.pop()
                        last_ans = memory.pop()

                        ans = run_op(last_op, last_ans, ans)
                        memory = []
                    else:
                        memory = []

                    memory.append(ans)
                    print ans
                else:
                    print "Error"
            # n1
            else:
                n1 = float(n1)
                if len(memory) == 0:
                    memory.append(n1)
                elif len(memory) == 1:
                    memory = []
                    memory.append(n1)
                elif len(memory) == 2:
                    last_op = memory.pop()
                    last_n = memory.pop()

                    ans = run_op(last_op, last_n, n1)
                    memory = []
                    memory.append(ans)
                    print ans
                else:
                    memory = []
                    print "Error"
        else:
            if op != None:
                memory.append(op)
            else:
                memory = []
                print "Error"

if __name__ == '__main__':
    main()