

def convert(number):
    answer = ""

    if number == 0:
        return number

    while number > 0:
        if number < 4000 and number >= 1000:
            answer += "M"
            number -= 1000
        elif number < 1000 and number >= 900:
            answer += "C"
            number += 100
        elif number < 900 and number >= 500:
            answer += "D"
            number -= 500
        elif number < 500 and number >= 400:
            answer += "C"
            number += 100
        elif number < 400 and number >= 100:
            answer += "C"
            number -= 100
        elif number < 100 and number >= 90:
            answer += "X"
            number += 10
        elif number < 90 and number >=50:
            answer += "L"
            number -= 50
        elif number <= 49 and number >= 40:
            answer += "X"
            number += 10
        elif number <= 39 and number >= 10:
            answer += "X"
            number -= 10
        elif number == 9:
            answer += "I"
            number += 1
        elif number < 9 and number >= 5:
            answer += "V"
            number -= 5
        elif number == 4:
            answer += "I"
            number += 1
        elif number < 4:
            answer += "I"
            number -= 1
        else:
            return "n < 4000 only"
    return answer