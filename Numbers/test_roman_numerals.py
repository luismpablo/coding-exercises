import unittest
import roman_numerals as rn

__author__ = 'LUISPABLO'


class TestRomanNumerals(unittest.TestCase):
    def test_zero(self):
        actual = rn.convert(0)
        expected = 0


        self.assertEqual(expected, actual, "Convert 0")

    def test_ones(self):
        actual = []
        for i in range(1, 10):
            actual.append(rn.convert(i))

        expected = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]

        self.assertEqual(expected, actual, "Convert ones")

    def test_tens(self):
        actual = []
        for i in range(10, 20):
            actual.append(rn.convert(i))

        expected = ["X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX"]

        self.assertEqual(expected, actual, "Convert 10-19")
if __name__ == '__main__':
    unittest.main()