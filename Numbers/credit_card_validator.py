def prepros(arr):
    for i in range(len(arr)):
        arr[i] = int(arr[i])

    return arr

def run_check(ccnum_arr):
    sum = 0
    checksum = ccnum_arr.pop()
    ccnum_arr.append(0)
    ccnum_arr.reverse()
    for i in range(len(ccnum_arr)):
        if (i + 1) % 2 == 0:
            temp = ccnum_arr[i] * 2
            if temp >= 10:
                ccnum_arr[i] = int(list(str(temp))[1]) + 1
            else:
                ccnum_arr[i] = temp

        sum += ccnum_arr[i]

    check_num = (sum * 9) % 10
    
    if checksum == check_num:
        return True

    return False

def main():
    ccnum = raw_input('Enter the Credit Card Number: ')
    try:
        int(ccnum)
    except:
        print "Invalid CC Number."
    else:
        ccnum_clean = prepros(list(ccnum))
        if run_check(ccnum_clean):
            print "This is a valid credit card number"
        else:
            print "This is NOT a valid credit card number"
        

if __name__ == '__main__':
    main()