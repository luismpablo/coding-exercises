class Trie:
    entries = dict()

    def __init__(self):
        self.entries = dict()

    def add(self, entry, root=None, key=0):
        if root == None:
            root = self.entries
        if len(entry) > 1:
            char = entry[0]

            if char not in root:
                root[char] = dict()
                root[char]["key"] = None
                root[char]["children"] = dict()

                self.add(entry[1:], root[char]["children"], key)
            else:
                self.add(entry[1:], root[char]["children"], key)
        else:
            char = entry
            if char not in root:
                root[char] = dict()
                root[char]["key"] = key
                root[char]["children"] = {}
            else:
                root[char]["key"] = key

    def remove(self, entry, root=None):
        if root == None:
            root = self.entries
        if len(entry) > 1:
            char = entry[0]
            if len(root[char]["children"]) > 1:
                self.remove(entry[1:], root[char]["children"])
            elif len(root[char]["children"]) == 1:
                self.remove(entry[1:], root[char]["children"])
                if len(root[char]["children"]) == 0:
                    del root[char]
            else:
                print False
        else:
            char = entry
            del root[char]
            return True


    def print_all(self, root=entries):
        pass