(defn collatz [x steps]
    (if (= x 1)
        (println steps)
        (if (= (mod x 2) 0)
            (collatz (/ x 2) (+ steps 1))
            (collatz (+ (* x 3) 1) (+ steps 1)))))

(let [x (read-line)]
    (collatz (read-string x) 0))