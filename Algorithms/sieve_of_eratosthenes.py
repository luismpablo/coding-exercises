number_list = []

def generate_numbers(n):
    for i in range(2, n + 1):
        number_list.append(i)

def check_if_prime(n):
    if n == 1: return True # special case
    elif n == 2: return True
    elif n == 3: return True
    else: return True

def remove_multiples(i, n):
    max_n = n / i
    for inc in range(2, max_n + 1):
        multiple = inc * i
        try:
            number_list.remove(multiple)
        except:
            pass

def main():
    try:
        ceiling = int(raw_input("Enter a number ceiling:"))
    except:
        print "Please enter a proper integer."
    else:
        generate_numbers(ceiling)
        for i in range(2, ceiling + 1):
            isPrime = check_if_prime(i)
            if isPrime: remove_multiples(i, ceiling + 1)

        # print len(number_list)
        # print number_list
        print sum(number_list)

if __name__ == '__main__':
    main()