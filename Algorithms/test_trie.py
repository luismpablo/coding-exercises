import unittest
from trie import Trie

__author__ = 'LUISPABLO'


class TestTrie(unittest.TestCase):
    def test_not_static(self):
        t = Trie()
        u = Trie()
        t.add("one")
        u.add("two")

        self.assertNotEqual(t, u, "Instances are non-static")

    def test_add_from_empty(self):
        t = Trie()
        t.add("one", key=30)

        expected = {
        "o": { "key": None, "children": { 
            "n": { "key": None, "children": { 
                "e": { "key": 30, "children": {}}}}}}}
        self.assertDictEqual(t.entries, expected, "Adding from an empty Trie")

    def test_add_no_collision(self):
        t = Trie()
        t.add("one", key=10)
        t.add("two", key=20)

        expected = dict(
          o=dict(
            key=None, 
            children=dict(
              n=dict(
                key=None, 
                children=dict(
                  e=dict(
                    key=10, 
                    children=dict()))))),
          t=dict(
            key=None,
            children=dict(
              w=dict(
                key=None,
                children=dict(
                  o=dict(
                    key=20,
                    children=dict()))))))

        self.assertEqual(expected, t.entries, "Adding a new entry without collision")

    def test_add_with_collision(self):
        t = Trie()
        t.add("ones", key=10)
        t.add("onus", key=15)

        expected = dict(
          o=dict(
            key=None, children=dict(
              n=dict(
                e=dict(
                  key=None, 
                  children=dict(
                    s=dict(key=10, children=dict()))), 
                u=dict(
                  key=None,
                  children=dict(
                    s=dict(key=15, children=dict())))))))

    def test_modify_key(self):
        t = Trie()
        t.add("one", key=30)
        t.add("on", key=50)

        expected = {
        "o": { "key": None, "children": { 
            "n": { "key": 50, "children": { 
                "e": { "key": 30, "children": {}}}}}}}
        self.assertDictEqual(t.entries, expected, "Modifying a key of an existing node")

    def test_remove_to_empty(self):
        t = Trie()
        t.add("one", key=30)
        t.remove("one")

        expected = {}
        self.assertDictEqual(t.entries, {}, "Removing the only entry")

    def test_remove_no_collision(self):
        t = Trie()
        t.add("one", key=30)
        t.add("two", key=50)
        t.remove("one")

        expected = dict(
          t=dict(
            key=None, children=dict(
              w=dict(
                key=None, children=dict(
                  o=dict(
                    key=50, children=dict()))))))

        self.assertDictEqual(t.entries, expected, "Removing an entry without collision")

    def test_remove_regular_collision(self):
        t = Trie()
        t.add("one", key=30)
        t.add("ona", key=50)
        t.remove("ona")

        expected = dict(
          o=dict(
            key=None, children=dict(
              n=dict(
                key=None, children=dict(
                  e=dict(
                    key=30, children=dict()))))))

        self.assertDictEqual(t.entries, expected, "Removing an entry")

    def test_remove_with_collision_without_key_modify(self):
        t = Trie()
        t.add("one", key=30)
        t.add("on", key=50)
        t.remove("one")

        expected = {
        "o": { "key": None, "children": { 
            "n": { "key": 50, "children": {}}}}}
        self.assertDictEqual(t.entries, expected, "Removing an entry with collision")

    def test_remove_with_collision_with_key_modify(self):
        t = Trie()
        t.add("one", key=30)
        t.add("on", key=50)
        t.remove("on")

        expected = {
        "o": { "key": None, "children": { 
            "n": { "key": None, "children": { 
                "e": { "key": 30, "children": {}}}}}}}
        self.assertDictEqual(t.entries, expected, "Removing an entry with collision and modifying an existing key")

if __name__ == '__main__':
    unittest.main()