import sys

def collatz(n, step):
    if n == 1:
        return step
    elif (n % 2) == 0:
        return collatz(n/2, step + 1)
    else:
        return collatz((3 * n) + 1, step + 1)

def main(num):
    try:
        n = int(num)
    except:
        print "Incorrect argument. Please enter a number."
    else:
        steps = collatz(n, 0)
        print steps

if __name__ == '__main__':
    try:
        main(sys.argv[1])
    except IndexError:
        print "Missing argument. 'python collatz.py [number]."
        pass