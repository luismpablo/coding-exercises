alphabet = "abcdefghijklmnopqrstuvwxyz "
table = dict()


def generate_caesar(shift):
    diff = shift
    alpha_count = len(alphabet)
    return alphabet[shift:alpha_count] + alphabet[0:diff]

def generate_table():
    i = 0
    for x in list(alphabet):
        table[x] = list(generate_caesar(i))
        i += 1

def generate_fullkey(key, msglen):
    fullkey = key
    keylen = len(key)
    

    diff = msglen / keylen
    for i in range(diff - 1):
        fullkey += key

    if (msglen - (diff * keylen)) > 0: fullkey = fullkey + key[0:(msglen % keylen)]

    return fullkey

def generate_cipher(fullkey, msg):
    encoded = ""
    for i in range(len(msg)):
        key = table[msg[i]]
        letter = alphabet.index(fullkey[i])
        encoded += key[letter]

    return encoded

def main():
    key = raw_input("Enter a keyword: ")
    msg = raw_input("Enter the message to encode: ")

    generate_table()
    fullkey = generate_fullkey(key, len(msg))

    print generate_cipher(fullkey, msg.lower())

if __name__ == '__main__':
    main()